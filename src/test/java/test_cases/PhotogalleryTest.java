package test_cases;

import components.DriverKiller;
import components.Navigator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.HomePage;
import pages.PhotogalleryPage;
import pages.SignInPage;

/**
 * Created by Petr Šindelář on 6/2/2016.
 */
public class PhotogalleryTest {

    private WebDriver driver;
    private DriverKiller killer;
    private SignInPage signInPage;
    private HomePage homePage;
    private Navigator navigator;
    private PhotogalleryPage photogalleryPage;

    @BeforeClass
    public void Setup() {

        driver = new FirefoxDriver();
        killer = new DriverKiller(driver);
        navigator = new Navigator(driver);
        signInPage = new SignInPage(driver);
        homePage = signInPage.loginValidUser();
        photogalleryPage = navigator.GoToPhotogalleryPage();
    }

    @AfterClass
    public void QuitDriver() {

        killer.DriverTearDown();
    }

    @Test
    public void CreateAlbum() {

        photogalleryPage.CreateAlbum();
        Assert.assertTrue(photogalleryPage.albumExist(), "Cannot find album! It should exist.");
    }

    @Test(dependsOnMethods = "CreateAlbum")
    public void DeleteAlbum() {

        photogalleryPage.DeleteAlbum();
        Assert.assertTrue(!photogalleryPage.albumExist(), "Test album has been found! It should not exist.");
    }
}
