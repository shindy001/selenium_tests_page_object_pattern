package test_cases;

import components.DriverKiller;
import components.Navigator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.NoticeBoardPage;
import pages.SignInPage;

/**
 * Created by shindy on 14.6.16.
 */
public class NoticeBoardTest {

    private WebDriver driver;
    private DriverKiller killer;
    private Navigator navigator;
    private HomePage homePage;
    private SignInPage signInPage;
    private NoticeBoardPage noticeBoardPage;

    @BeforeClass
    public void Setup() {

        driver = new FirefoxDriver();
        killer = new DriverKiller(driver);
        navigator = new Navigator(driver);
        signInPage = new SignInPage(driver);
        homePage = signInPage.loginValidUser();
        noticeBoardPage = navigator.GoToNoticeBoardPage();
    }

    @AfterClass
    public void QuitDriver() {

        killer.DriverTearDown();
    }

    @Test
    public void CreateNotice() {

        noticeBoardPage.CreateNotice();
        Assert.assertTrue(noticeBoardPage.noticePostExists(), "Cannot find Notice! It should exist.");
    }

    @Test
    public void DeleteNotice() {

        noticeBoardPage.DeleteNotice();
        Assert.assertTrue(!noticeBoardPage.noticePostExists(), "Notice has been found! It should not exist.");
    }
}
