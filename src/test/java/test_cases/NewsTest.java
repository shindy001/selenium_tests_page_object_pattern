package test_cases;

import components.DriverKiller;
import components.Navigator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.HomePage;
import pages.NewsPage;
import pages.SignInPage;

/**
 * Created by Petr Šindelář on 6/10/2016.
 */
public class NewsTest {

    private WebDriver driver;
    private DriverKiller killer;
    private SignInPage signInPage;
    private HomePage homePage;
    private Navigator navigator;
    private NewsPage newsPage;

    @BeforeClass
    public void Setup() {

        driver = new FirefoxDriver();
        killer = new DriverKiller(driver);
        navigator = new Navigator(driver);
        signInPage = new SignInPage(driver);
        homePage = signInPage.loginValidUser();
        newsPage = navigator.GoToNewsPage();
    }

    @AfterClass
    public void QuitDriver() {

        killer.DriverTearDown();
    }

    @Test(priority = 0)
    public void CreateNewsPost() {

        newsPage.CreateNewsPost();
        Assert.assertTrue(newsPage.newsPostExist(), "Cannot find News Post! It should exist.");
    }

    @Test(priority = 1)
    public void DeleteNewsPost() {

        newsPage.DeleteNewsPost();
        Assert.assertTrue(!newsPage.newsPostExist(), "News Post has been found! It should not exist.");
    }

    @Test(priority = 2)
    public void CreateConceptPost() {

        newsPage.CreateConcept();
        Assert.assertTrue(newsPage.conceptExist(), "Cannot find Concept Post! It should exist.");
    }

    @Test(priority = 3)
    public void DeleteConceptPost() {

        newsPage.DeleteConcept();
        Assert.assertTrue(!newsPage.conceptExist(), "Concept Post has been found! It should not exist.");
    }



}
