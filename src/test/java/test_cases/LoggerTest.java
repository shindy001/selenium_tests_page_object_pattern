package test_cases;

import components.DriverKiller;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.HomePage;
import pages.SignInPage;

/**
 * Created by Petr Šindelář on 5/26/2016.
 */
public class LoggerTest {

    private WebDriver driver;
    private SignInPage signInPage;
    private HomePage homePage;
    private DriverKiller killer;

    @BeforeClass
    private void Setup() {

        driver = new FirefoxDriver();
        killer = new DriverKiller(driver);
    }

    @AfterClass
    public void QuitDriver() {

        killer.DriverTearDown();
    }

    @Test
    public void LoginTest() {

        signInPage = new SignInPage(driver);
        homePage = signInPage.loginValidUser();
        Assert.assertTrue(signInPage.isLoggedIn(), "Login was unsuccessful");
    }

    @Test(dependsOnMethods = "LoginTest")
    public void LogoutTest() {

       signInPage = signInPage.logoutValidUser();
    }
}
