package components;

import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 6/7/2016.
 */
public class DriverKiller {

    private WebDriver driver;

    public DriverKiller(WebDriver driver) {

        this.driver = driver;
    }

    public void DriverTearDown() {

        driver.close();
        driver.quit();
    }
}
