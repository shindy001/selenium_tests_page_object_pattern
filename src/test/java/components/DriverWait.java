package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Petr Šindelář on 6/2/2016.
 */
public class DriverWait {

    private WebDriver driver;
    private WebDriverWait wait;

    public DriverWait(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public void WaitForElementToAppear(By locator) {

        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void WaitASec() {

        try{
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
