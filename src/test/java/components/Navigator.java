package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.NewsPage;
import pages.NoticeBoardPage;
import pages.PhotogalleryPage;

/**
 * Created by Petr Šindelář on 6/7/2016.
 */
public class Navigator {

    private WebDriver driver;
    private Clicker clicker;
    private DriverWait wait;

    public  Navigator(WebDriver driver) {

        this.driver = driver;
        clicker = new Clicker(driver);
        wait = new DriverWait(driver);
    }

    public enum Page {

        Home, Photogallery, News, NoticeBoard,Catalogue
    }

    public void GoBack() {

        By backBtnSelector = By.cssSelector(".toolbar .fa-reply");
        wait.WaitForElementToAppear(backBtnSelector);
        driver.findElement(backBtnSelector).click();
    }

    public void GoToNewPostForm() {

        By addPostBtnSelector = By.cssSelector(".toolbar .fa-plus");
        wait.WaitForElementToAppear(addPostBtnSelector);
        driver.findElement(addPostBtnSelector).click();
    }

    public PhotogalleryPage GoToPhotogalleryPage() {

        GoTo(Page.Photogallery);
        return new PhotogalleryPage(driver);
    }

    public NewsPage GoToNewsPage() {

        GoTo(Page.News);
        return new NewsPage(driver);
    }

    public HomePage GoToHomePage() {

        GoTo(Page.Home);
        return new HomePage(driver);
    }

    public NoticeBoardPage GoToNoticeBoardPage() {

        GoTo(Page.NoticeBoard);
        return new NoticeBoardPage(driver);
    }

    private void GoTo(Page page) {

        wait.WaitForElementToAppear(CastValue(page));
        driver.findElement(CastValue(page)).click();
    }

    private By CastValue(Page page) {

        switch (page) {
            case Home:
                return By.id("ipomenu5006418");
            case Photogallery:
                return By.id("ipomenu4811883");
            case News:
                return By.id("ipomenu4811884");
            case NoticeBoard:
                return By.id("ipomenu4922663");
            case Catalogue:
                return By.id("ipomenu4907731");
        }
        return null;
    }
}
