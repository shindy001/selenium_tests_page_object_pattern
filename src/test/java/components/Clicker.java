package components;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Petr Šindelář on 6/2/2016.
 */
public class Clicker {

    private WebDriver driver;
    private DriverWait wait;

    public Clicker(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
    }

    public enum FindBy {
        Name, Id, CssSelector, CssClass, LinkTxt, PartialLinkTxt, Xpath
    }

    public void ClickAndWait(FindBy findBy, String value) {

        wait.WaitForElementToAppear(CastValue(findBy, value));
        driver.findElement(CastValue(findBy, value)).click();
        SearchForElementTillYouFindIt();
    }

    private By CastValue(FindBy findBy, String value) {

        switch (findBy) {
            case Name:
                return By.name(value);
            case Id:
                return By.id(value);
            case CssSelector:
                return By.cssSelector(value);
            case CssClass:
                return By.className(value);
            case LinkTxt:
                return By.linkText(value);
            case PartialLinkTxt:
                return By.partialLinkText(value);
            case Xpath:
                return By.xpath(value);
        }
        return null;
    }

    private void SearchForElementTillYouFindIt() {

        try {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        catch (NoSuchElementException e) {
            System.err.println("Cannot find element location");
        }
    }

}
