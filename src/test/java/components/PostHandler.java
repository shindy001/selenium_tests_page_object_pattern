package components;

import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 6/8/2016.
 */
public class PostHandler {

    private WebDriver driver;
    private DriverWait wait;
    private Clicker clicker;

    public PostHandler(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
        clicker = new Clicker(driver);
    }

    public void SavePost() {

        By saveBtnSelector = By.cssSelector(".toolbar .fa-save");
        wait.WaitForElementToAppear(saveBtnSelector);

        try {
            driver.findElement(saveBtnSelector).click();
        } catch (UnhandledAlertException e) {
            //Do nothing
            }
    }

    public void SaveConceptPost() {
        By saveConceptBtn = By.className("fa-file-text-o");
        wait.WaitForElementToAppear(saveConceptBtn);
        driver.findElement(saveConceptBtn).click();
    }

    public void DeletePost() {

        By deleteBtnSelector = By.cssSelector(".toolbar .fa-trash-o");
        By deleteBtnSelector2 = By.cssSelector(".toolbar .fa-trash");
        By areYouSureWindow = By.cssSelector(".ui-dialog-buttonset button:first-child");

        wait.WaitForElementToAppear(By.className("toolbar"));

        if(driver.findElements(deleteBtnSelector).size() > 0 ) {
            wait.WaitForElementToAppear(deleteBtnSelector);
            driver.findElement(deleteBtnSelector).click();

        } else {
            wait.WaitForElementToAppear(deleteBtnSelector2);
            driver.findElement(deleteBtnSelector2).click();
        }

        if(driver.findElements(areYouSureWindow).size() > 0)
            driver.findElement(areYouSureWindow).click();
    }
}
