package components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Petr Šindelář on 6/2/2016.
 */
public class TxtFieldFiller {

    private WebDriver driver;
    private DriverWait wait;

    public TxtFieldFiller(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
    }

    public void FillTextField(String fieldName, String inputValue) {

        By name = By.name(fieldName);

        wait.WaitForElementToAppear(name);
        WebElement textField = driver.findElement(name);
        textField.clear();
        textField.sendKeys(inputValue);
    }
}
