package components;

import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 6/1/2016.
 */
public class URLLocator {

    private WebDriver driver;
    private DriverWait wait;

    public URLLocator(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
    }

    public void GoToURL(String url) {

        driver.get(url);
    }
}
