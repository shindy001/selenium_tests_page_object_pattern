package pages;

import components.Clicker;
import components.DriverWait;
import components.Navigator;
import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 5/26/2016.
 */
public class HomePage {

    private WebDriver driver;
    private DriverWait wait;
    private Clicker clicker;
    private Navigator navigator;

    public HomePage(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
        clicker = new Clicker(driver);
        navigator = new Navigator(driver);

        if(!driver.getTitle().equals("Selenium tests")) {
            throw new IllegalStateException("This is not Home Page, " +
                    "current is: " +driver.getCurrentUrl());
        }
        driver.navigate().refresh();
    }

    public HomePage manageProfile() {

        return new HomePage(driver);
    }
}
