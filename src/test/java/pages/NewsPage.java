package pages;

import components.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 6/10/2016.
 */
public class NewsPage {

    private WebDriver driver;
    private DriverWait wait;
    private PostHandler postHandler;
    private TxtFieldFiller filler;
    private Navigator navigator;
    private Clicker clicker;
    private String newsPostName = "Test news post";

    public NewsPage (WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
        postHandler = new PostHandler(driver);
        filler = new TxtFieldFiller(driver);
        navigator = new Navigator(driver);
        clicker = new Clicker(driver);

        if (!driver.getTitle().equals("News | Selenium tests")) {
            throw new IllegalStateException("This is not News Page, " +
                    "current is: " + driver.getCurrentUrl());
        }
    }

    public void CreateNewsPost() {

        navigator.GoToNewPostForm();
        filler.FillTextField("title", newsPostName);
        postHandler.SavePost();
    }

    public void CreateConcept() {

        navigator.GoToNewPostForm();
        filler.FillTextField("title", newsPostName);
        postHandler.SaveConceptPost();
    }

    public void DeleteNewsPost() {

        clicker.ClickAndWait(Clicker.FindBy.LinkTxt, newsPostName);
        postHandler.DeletePost();
    }

    public void DeleteConcept() {

        GoToNewsConceptPage();
        clicker.ClickAndWait(Clicker.FindBy.LinkTxt, newsPostName);
        postHandler.DeletePost();
    }

    public boolean newsPostExist() {

        navigator.GoToNewsPage();

        try {
            return driver.findElements(By.linkText(newsPostName)).size() > 0;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean conceptExist() {

        navigator.GoToNewsPage();

        try {
            return driver.findElements(By.className("fa-briefcase")).size() > 0;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private void GoToNewsConceptPage() {

        By conceptBtn = By.className("fa-briefcase");
        wait.WaitForElementToAppear(conceptBtn);

        if(driver.findElements(conceptBtn).size() > 0 ) {
            driver.findElement(conceptBtn).click();
        }
    }
}
