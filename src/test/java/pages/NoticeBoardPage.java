package pages;

import components.Clicker;
import components.Navigator;
import components.PostHandler;
import components.TxtFieldFiller;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

/**
 * Created by shindy on 14.6.16.
 */
public class NoticeBoardPage {

    private WebDriver driver;
    private Clicker clicker;
    private Navigator navigator;
    private PostHandler postHandler;
    private TxtFieldFiller filler;
    private String noticeName = "Test notice";

    public NoticeBoardPage(WebDriver driver) {

        this.driver = driver;
        clicker = new Clicker(driver);
        navigator = new Navigator(driver);
        postHandler = new PostHandler(driver);
        filler = new TxtFieldFiller(driver);

        if (!driver.getTitle().equals("Notice board | Selenium tests")) {
            throw new IllegalStateException("This is not Notice board Page, " +
                    "current is: " + driver.getCurrentUrl());
        }
    }

    public void CreateNotice() {

        navigator.GoToNewPostForm();
        filler.FillTextField("name", noticeName);
        postHandler.SavePost();
    }

    public void DeleteNotice() {

        clicker.ClickAndWait(Clicker.FindBy.LinkTxt, noticeName);
        postHandler.DeletePost();
    }

    public boolean noticePostExists() {

        navigator.GoToNoticeBoardPage();

        try {
          return driver.findElements(By.linkText(noticeName)).size() > 0;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
