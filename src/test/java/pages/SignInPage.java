package pages;

import components.*;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Petr Šindelář on 5/26/2016.
 */
public class SignInPage {

    private WebDriver driver;
    private DriverWait wait;
    private Clicker clicker;
    private TxtFieldFiller filler;
    private URLLocator locator;
    private WebElement element;
    private Actions action;
    private boolean signInPagePass = false;
    private final String username = "seleniumtest";
    private final String password = "147852Ab12";

    public SignInPage(WebDriver driver) {

        this.driver = driver;
        wait = new DriverWait(driver);
        clicker = new Clicker(driver);
        filler = new TxtFieldFiller(driver);
        locator = new URLLocator(driver);
        action = new Actions(driver);

        if(!signInPagePass)
            driver.manage().window().maximize();
            locator.GoToURL("http://ipo.antee.cz");

        if (!driver.getTitle().equals("Přihlášení do IPO")) {
            throw new IllegalStateException("This is not Sign in page, current page is: "
                    +driver.getCurrentUrl());
        }
        signInPagePass = true;
    }

    public HomePage loginValidUser() {

        filler.FillTextField("username", username);
        filler.FillTextField("password", password);
        clicker.ClickAndWait(Clicker.FindBy.Name, "login");
        wait.WaitForElementToAppear(By.id("system-menu"));

        return new HomePage(driver);
    }

    public SignInPage logoutValidUser() {

        wait.WaitForElementToAppear(By.className("account-menu-user"));
        element = driver.findElement(By.className("account-menu-user"));
        action.moveToElement(element).build().perform();
        clicker.ClickAndWait(Clicker.FindBy.Xpath,"//*[@id=\"accountMenu\"]/li[2]/ul/li[5]/a");

        return new SignInPage(driver);
    }

    public boolean isLoggedIn() {

        By sysMenuId = By.id("system-menu");

        try {
            wait.WaitForElementToAppear(sysMenuId);
            return driver.findElements(sysMenuId).size() > 0;
        }
        catch (TimeoutException e) {
            return false;
        }
    }
}
