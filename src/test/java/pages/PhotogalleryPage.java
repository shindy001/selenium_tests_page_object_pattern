package pages;

import components.Clicker;
import components.Navigator;
import components.PostHandler;
import components.TxtFieldFiller;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

/**
 * Created by Petr Šindelář on 6/1/2016.
 */
public class PhotogalleryPage {

    private WebDriver driver;
    private Clicker clicker;
    private Navigator navigator;
    private PostHandler postHandler;
    private TxtFieldFiller filler;
    private String albumName = "Test album";
;
    public PhotogalleryPage(WebDriver driver) {

        this.driver = driver;
        clicker = new Clicker(driver);
        navigator = new Navigator(driver);
        postHandler = new PostHandler(driver);
        filler = new TxtFieldFiller(driver);

        if (!driver.getTitle().equals("Photogallery | Selenium tests")) {
            throw new IllegalStateException("This is not Photogalerry Page, " +
                    "current is: " + driver.getCurrentUrl());
        }
    }

    public void CreateAlbum() {

        navigator.GoToNewPostForm();
        filler.FillTextField("name", albumName);
        postHandler.SavePost();
    }

    public void DeleteAlbum() {

        clicker.ClickAndWait(Clicker.FindBy.LinkTxt, albumName);
        postHandler.DeletePost();
    }

    public boolean albumExist() {

        navigator.GoToPhotogalleryPage();

        try {
            return driver.findElements(By.linkText(albumName)).size() > 0;

        } catch(NoSuchElementException e) {
            return false;
        }
    }
}
